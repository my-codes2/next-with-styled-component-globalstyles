import GlobalStyle from "./../styles/global";

export default function Home() {
  return (
    <>
      <GlobalStyle />
      <div>
        <h1>Hello World</h1>
      </div>
    </>
  );
}
