## Modelo NextJS , Styled Component usando global style

Foi criado um modelo de um fonte onde posso utilizar o NextJS com Styled Component, acessando
um arquivo separado com um estilo global e ainda sim posso utilizar estilos individuais para cada
componente criado;

## Getting Started

Primeiro suba o servidor de desenvolvimento :

```bash
npm run dev
# or
yarn dev
```

Ele vai iniciar neste endereço [http://localhost:3000](http://localhost:3000) abra-o no browser.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js](https://nextjs.org)

- [Styled Component](https://nextjs.org/learn)

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel
