import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
 h1 {
   font-size: 4rem;
 }
`;

export default GlobalStyle;
